#include <libndls.h>
#include <ngc.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define PLAYER_SIZE 10
#define NUM_ASTEROIDS 10
#define ASTEROID_DETAIL 5
#define BULLET_SPEED 3

struct vectorf{
	float x;
	float y;
};

struct asteroid{
	struct vectorf pos;
	struct vectorf velocity;
	float rotation;
	float rVelocity;
	struct vectorf points[ASTEROID_DETAIL];
};

struct player{
	struct vectorf pos;
	struct vectorf velocity;
	float rotation;
};

struct bullet{
	struct vectorf pos;
	struct vectorf velocity;
	struct bullet* next;
};

int readNumFromFile(char* filename){
	FILE *f = fopen(filename, "r");
	if(f == NULL){
		f = fopen(filename, "w");
		fprintf(f, "0");
		fclose(f);
		return 0;
	}
	int out;
	
	fscanf(f, "%d", &out);
	fclose(f);

	return out;
}

void writeNumToFile(char* filename, int num){
	FILE *f = fopen(filename, "w");
	fprintf(f, "%d", num);
	fclose(f);
}

void seed(){
	//read seed from file
	FILE *f = fopen("seed.tns", "r");
	if(f == NULL){
		f = fopen("seed.tns", "w");
		fprintf(f, "0");
		fclose(f);
		
		f = fopen("seed.tns", "r");
	}
	int seed;
	fscanf(f, "%d", &seed);
	fclose(f);
	
	//add one to seed and then write it back to the file
	seed += 1;
	
	f = fopen("seed.tns", "w");
	fprintf(f, "%d", seed);
	fclose(f);
	
	srand(seed);
}

float xEdge(float r){
	if(r < 0.25){
		return (r*4.0)*((float)SCREEN_WIDTH);
	}else if(r < 0.5){
		return (float)SCREEN_WIDTH+10.0;
	}else if(r < 0.75){
		return ((r-0.5)*-4.0)*((float)SCREEN_WIDTH);
	}else{
		return -10.0;
	}
}

float yEdge(float r){
	if(r < 0.25){
		return -10.0;
	}else if(r < 0.5){
		return ((r-0.25)*4.0)*((float)SCREEN_HEIGHT);
	}else if(r < 0.75){
		return (float)SCREEN_HEIGHT+10.0;
	}else{
		return ((r-0.75)*-4)*((float)SCREEN_HEIGHT);
	}
}

//returns a random float between 0 and 1
float randf(){
	return ((float)rand()/(float)(RAND_MAX));
}

//returns the squared distance between 2 points
float distanceSquared(struct vectorf a, struct vectorf b){
	float x = a.x-b.x;
	float y = a.y-b.y;
	return x*x+y*y;
}

struct asteroid newAsteroid(){
	struct asteroid new;
	//initialize all the variables
	float r = randf();
	new.pos.x = xEdge(r);
	new.pos.y = yEdge(r);
	float dir = randf()*(M_PI*2);
	float speed = randf()*0.5+0.5;
	new.velocity.x = sin(dir)*speed;
	new.velocity.y = cos(dir)*speed;
	new.rotation = randf()*M_PI*2;
	new.rVelocity = randf()*0.01-0.005;
	
	//we are using polar coordinates for the points, x = distance, y = angle
	for(int i = 0; i < ASTEROID_DETAIL; i++){
		new.points[i].x = randf()*10+5;
		new.points[i].y = (2*M_PI)*((((float)i)+1)/ASTEROID_DETAIL);
	}
	
	return new;
}

int main(int _argc, char* argv[]){
	enable_relative_paths(argv);
	seed();
	
	Gc gc = gui_gc_global_GC();
	gui_gc_setRegion(gc, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	gui_gc_begin(gc);
	
	struct player player;
	player.pos.x = SCREEN_WIDTH/2;
	player.pos.y = SCREEN_HEIGHT/2;
	player.velocity.x = 0;
	player.velocity.y = 0;
	player.rotation = 0;
	
	struct asteroid asteroids[NUM_ASTEROIDS];
	for(int i = 0; i < NUM_ASTEROIDS; i++){
		asteroids[i] = newAsteroid();
	}
	
	struct bullet* bullets = NULL;
	bool prevShootPressed = false;
	
	char line1[] = "You Died!";
	char line116[20];
	ascii2utf16(line116, line1, 20);
	int len1 = gui_gc_getStringWidth(gc, SerifRegular10, line116, 0, 9);
	
	while(1){
		//draw bg
		gui_gc_setColorRGB(gc, 0, 0, 0);
		gui_gc_fillRect(gc, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		
		//asteroids
		gui_gc_setColorRGB(gc, 255, 255, 255);
		for(int i = 0; i < NUM_ASTEROIDS; i++){
			//make a new asteroid if out of bounds
			if(asteroids[i].pos.x < -10 || asteroids[i].pos.x > SCREEN_WIDTH+10 || asteroids[i].pos.y < -10 || asteroids[i].pos.y > SCREEN_HEIGHT+10){
				asteroids[i] = newAsteroid();
			}
			//asteroid physics
			asteroids[i].pos.x += asteroids[i].velocity.x;
			asteroids[i].pos.y += asteroids[i].velocity.y;
			asteroids[i].rotation += asteroids[i].rVelocity;
			//draw asteroid
			for(int j = 0; j < ASTEROID_DETAIL; j++){
				gui_gc_drawLine(gc,
						(int)(asteroids[i].pos.x+(sin(asteroids[i].rotation+asteroids[i].points[j].y)*asteroids[i].points[j].x)),
						(int)(asteroids[i].pos.y+(cos(asteroids[i].rotation+asteroids[i].points[j].y)*asteroids[i].points[j].x)),
						(int)(asteroids[i].pos.x+(sin(asteroids[i].rotation+asteroids[i].points[(j+1)%ASTEROID_DETAIL].y)*asteroids[i].points[(j+1)%ASTEROID_DETAIL].x)),
						(int)(asteroids[i].pos.y+(cos(asteroids[i].rotation+asteroids[i].points[(j+1)%ASTEROID_DETAIL].y)*asteroids[i].points[(j+1)%ASTEROID_DETAIL].x)));
			}
		}
		
		//player physics
		player.pos.x += player.velocity.x;//apply velocity
		player.pos.y += player.velocity.y;
		player.velocity.x *= 0.995;//friction
		player.velocity.y *= 0.995;
		//rotation inputs
		if(isKeyPressed(KEY_NSPIRE_LEFT) || isKeyPressed(KEY_NSPIRE_LEFTUP) || isKeyPressed(KEY_NSPIRE_4)){
			player.rotation += 0.1;
		}
		if(isKeyPressed(KEY_NSPIRE_RIGHT) || isKeyPressed(KEY_NSPIRE_UPRIGHT) || isKeyPressed(KEY_NSPIRE_6)){
			player.rotation -= 0.1;
		}
		//thrust!
		if(isKeyPressed(KEY_NSPIRE_UP) || isKeyPressed(KEY_NSPIRE_UPRIGHT) || isKeyPressed(KEY_NSPIRE_LEFTUP) || isKeyPressed(KEY_NSPIRE_8)){
			player.velocity.x += sin(player.rotation)*0.01;
			player.velocity.y += cos(player.rotation)*0.01;
			//draw flames
			gui_gc_setColorRGB(gc, 255, 255, 255);
			int flameSize = rand()%5+3;
			gui_gc_drawLine(gc, (int)(player.pos.x+(sin(player.rotation+(2.66))*PLAYER_SIZE*.8)), (int)(player.pos.y+(cos(player.rotation+(2.66))*PLAYER_SIZE*.8)),
					(int)(player.pos.x+(sin(player.rotation+M_PI)*(PLAYER_SIZE+flameSize))), (int)(player.pos.y+(cos(player.rotation+M_PI)*(PLAYER_SIZE+flameSize))));
			gui_gc_drawLine(gc, (int)(player.pos.x+(sin(player.rotation+(-2.66))*PLAYER_SIZE*.8)), (int)(player.pos.y+(cos(player.rotation+(-2.66))*PLAYER_SIZE*.8)),
					(int)(player.pos.x+(sin(player.rotation+M_PI)*(PLAYER_SIZE+flameSize))), (int)(player.pos.y+(cos(player.rotation+M_PI)*(PLAYER_SIZE+flameSize))));
		}
		//shooting
		if(!prevShootPressed && (isKeyPressed(KEY_NSPIRE_5) || isKeyPressed(KEY_NSPIRE_CLICK))){
			//add a bullet
			struct bullet* new = malloc(sizeof(struct bullet));
			new->pos = player.pos;
			new->velocity.x = sin(player.rotation)*BULLET_SPEED;
			new->velocity.y = cos(player.rotation)*BULLET_SPEED;
			new->pos.x += sin(player.rotation)*PLAYER_SIZE;
			new->pos.y += cos(player.rotation)*PLAYER_SIZE;
			new->next = NULL;
			if(bullets == NULL){
				bullets = new;
			}else{
				struct bullet* cur = bullets;
				while(cur->next != NULL){
					cur = cur->next;
				}
				cur->next = new;
			}
		}
		prevShootPressed = (isKeyPressed(KEY_NSPIRE_5) || isKeyPressed(KEY_NSPIRE_CLICK));
		//bullets
		struct bullet* cur = bullets;
		struct bullet* prev = NULL;
		while(cur != NULL){
			if(cur->pos.x < 0 || cur->pos.x > SCREEN_WIDTH || cur->pos.y < 0 || cur->pos.y > SCREEN_HEIGHT){
				//delete this bullet
				if(prev != NULL){
					prev->next = cur->next;
					free(cur);
					cur = prev->next;
				}else{
					struct bullet* tmp = cur;
					bullets = cur->next;
					free(tmp);
					break;
				}
				continue;
			}
			//apply velocity
			cur->pos.x += cur->velocity.x;
			cur->pos.y += cur->velocity.y;
			//check for collisions with asteroids
			for(int i = 0; i < NUM_ASTEROIDS; i++){
				if(distanceSquared(asteroids[i].pos, cur->pos) < 100){
					//overwrite the asteroid with a new one
					asteroids[i] = newAsteroid();
				}
			}
			//draw bullet
			gui_gc_setColorRGB(gc, 255, 255, 255);
			gui_gc_drawLine(gc,
					(int)(cur->pos.x),
					(int)(cur->pos.y),
					(int)(cur->pos.x+cur->velocity.x),
					(int)(cur->pos.y+cur->velocity.y));
			prev = cur;
			cur = cur->next;
		}
		//loop player rotation
		while(player.rotation < 0){
			player.rotation += 2*M_PI;
		}
		while(player.rotation > 2*M_PI){
			player.rotation -= 2*M_PI;
		}
		//walls
		if((int)player.pos.x > SCREEN_WIDTH){
			player.pos.x = 0;
		}else if((int)player.pos.x < 0){
			player.pos.x = (float)SCREEN_WIDTH;
		}
		if((int)player.pos.y > SCREEN_HEIGHT){
			player.pos.y = 0;
		}else if((int)player.pos.y < 0){
			player.pos.y = (float)SCREEN_HEIGHT;
		}
		//quit
		if(isKeyPressed(KEY_NSPIRE_ESC)){
			break;
		}
		//draw player
		gui_gc_setColorRGB(gc, 255, 255, 255);
		gui_gc_drawLine(gc, (int)(player.pos.x+(sin(player.rotation)*PLAYER_SIZE)), (int)((player.pos.y)+(cos(player.rotation)*PLAYER_SIZE)),
				(int)(player.pos.x+sin(player.rotation+((9*M_PI)/12))*PLAYER_SIZE), (int)(player.pos.y+cos(player.rotation+((9*M_PI)/12))*PLAYER_SIZE));
		gui_gc_drawLine(gc, (int)(player.pos.x+(sin(player.rotation+((15*M_PI)/12))*PLAYER_SIZE)), (int)((player.pos.y)+(cos(player.rotation+((15*M_PI)/12))*PLAYER_SIZE)),
				(int)(player.pos.x+sin(player.rotation+((9*M_PI)/12))*PLAYER_SIZE), (int)(player.pos.y+cos(player.rotation+((9*M_PI)/12))*PLAYER_SIZE));
		gui_gc_drawLine(gc, (int)(player.pos.x+(sin(player.rotation)*PLAYER_SIZE)), (int)((player.pos.y)+(cos(player.rotation)*PLAYER_SIZE)),
				(int)(player.pos.x+sin(player.rotation+((15*M_PI)/12))*PLAYER_SIZE), (int)(player.pos.y+cos(player.rotation+((15*M_PI)/12))*PLAYER_SIZE));
		//check for player death
		for(int i = 0; i < NUM_ASTEROIDS; i++){
			if(distanceSquared(asteroids[i].pos, player.pos) < 100){
				//player died
				gui_gc_drawString(gc, line116, (SCREEN_WIDTH/2)-len1, SCREEN_HEIGHT/2, GC_SM_TOP);
				//reset the game
				for(int i = 0; i < NUM_ASTEROIDS; i++){
					asteroids[i] = newAsteroid();
				}
				player.rotation = 0;
				player.pos.x = SCREEN_WIDTH/2;
				player.pos.y = SCREEN_HEIGHT/2;
				
				while(bullets != NULL){
					struct bullet* tmp = bullets;
					bullets = bullets->next;
					free(tmp);
				}
				
				gui_gc_blit_to_screen(gc);
				wait_key_pressed();
			}
		}
		//update the screen
		gui_gc_blit_to_screen(gc);
	}
	while(bullets != NULL){
		struct bullet* tmp = bullets;
		bullets = bullets->next;
		free(tmp);
	}
	gui_gc_finish(gc);
	return 0;
}
